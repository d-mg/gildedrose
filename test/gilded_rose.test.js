const { Shop, Item, ConjuredItem } = require('../src/gilded_rose');

describe('Gilded Rose', function () {
    describe('Item', function () {
        const NAME = 'NAME';
        const SELL_IN = 'SELL_IN';
        const QUALITY = 'QUALITY';

        it('should have name', function () {
            const item = new Item(NAME, SELL_IN, QUALITY);
            expect(item.name).toBe(NAME);
        });

        it('should have sellIn', function () {
            const item = new Item(NAME, SELL_IN, QUALITY);
            expect(item.sellIn).toBe(SELL_IN);
        });

        it('should have quality', function () {
            const item = new Item(NAME, SELL_IN, QUALITY);
            expect(item.quality).toBe(QUALITY);
        });
    });

    describe('ConjuredItem', function () {
        const NAME = 'NAME';
        const SELL_IN = 'SELL_IN';
        const QUALITY = 'QUALITY';

        it('should have name', function () {
            const item = new ConjuredItem(NAME, SELL_IN, QUALITY);
            expect(item.name).toBe(NAME);
        });

        it('should have sellIn', function () {
            const item = new ConjuredItem(NAME, SELL_IN, QUALITY);
            expect(item.sellIn).toBe(SELL_IN);
        });

        it('should have quality', function () {
            const item = new ConjuredItem(NAME, SELL_IN, QUALITY);
            expect(item.quality).toBe(QUALITY);
        });

        it('should have multiplier 2', function () {
            const item = new ConjuredItem(NAME, SELL_IN, QUALITY);
            expect(item.multiplier).toBe(2);
        });
    });

    describe('shop', function () {
        const NAME = 'NAME';

        it('should handle no item\'s', function () {
            const shop = new Shop();
            const items = shop.updateQuality();
            expect(Array.isArray(items)).toBe(true);
            expect(items.length).toBe(0);
        });

        it('should have item\'s', function () {
            const item1 = new Item(NAME, 1, 1);
            const item2 = new Item(NAME, 1, 1);
            const shop = new Shop([item1, item2]);
            const items = shop.updateQuality();
            expect(items[0]).toBe(item1);
            expect(items[1]).toBe(item2);
        });

        describe('upgradeQuality()', function () {
            const AGED_BRIE = 'Aged Brie';
            const SULFURAS = 'Sulfuras, Hand of Ragnaros';
            const BACKSTAGE_PASSES = 'Backstage passes to a TAFKAL80ETC concert';

            const setupItem = setupAnItem(Item);
            const setupConjuredItem = setupAnItem(ConjuredItem);

            function setupAnItem(AnItem) {
                return function setupAnItemInner(name, sellIn, quality) {
                    const item = new AnItem(name, sellIn, quality);
                    const shop = new Shop([item]);
                    return shop.updateQuality()[0];
                }
            }

            describe('Item', function () {
                describe('sellIn', function () {
                    it('should decrease by 1', function () {
                        const item = setupItem(NAME, 1, 1);
                        expect(item.sellIn).toBe(0);
                    });

                    it('should go negative', function () {
                        const item = setupItem(NAME, 0, 1);
                        expect(item.sellIn).toBe(-1);
                    });
                });

                describe('quality', function () {
                    it('should decrease by 1', function () {
                        const item = setupItem(NAME, 1, 2);
                        expect(item.quality).toBe(1);
                    });

                    it('should not go negative', function () {
                        const item = setupItem(NAME, 1, 0);
                        expect(item.quality).toBe(0);
                    });

                    describe('sell date passed', function () {
                        it('should decrease by 2', function () {
                            const item = setupItem(NAME, 0, 3);
                            expect(item.quality).toBe(1);
                        });
                    });
                });

                describe(`named "${AGED_BRIE}"`, function () {
                    describe('sellIn', function () {
                        it('should decrease by 1', function () {
                            const item = setupItem(NAME, 1, 1);
                            expect(item.sellIn).toBe(0);
                        });

                        it('should go negative', function () {
                            const item = setupItem(NAME, 0, 1);
                            expect(item.sellIn).toBe(-1);
                        });
                    });

                    describe('quality', function () {
                        it('should increase by 1', function () {
                            const item = setupItem(AGED_BRIE, 1, 1);
                            expect(item.quality).toBe(2);
                        });

                        it('should not go over 50', function () {
                            const item = setupItem(AGED_BRIE, 1, 50);
                            expect(item.quality).toBe(50);
                        });

                        describe('sell date passed', function () {
                            it('should increase by 2', function () {
                                const item = setupItem(AGED_BRIE, 0, 1);
                                expect(item.quality).toBe(3);
                            });
                        });
                    });
                });

                describe(`named "${SULFURAS}"`, function () {
                    describe('sellIn', function () {
                        it('should not change', function () {
                            const item = setupItem(SULFURAS, 1, 1);
                            expect(item.sellIn).toBe(1);
                        });
                    });

                    describe('quality', function () {
                        it('should not change', function () {
                            const item = setupItem(SULFURAS, 1, 1);
                            expect(item.quality).toBe(1);
                        });
                    });
                });

                describe(`named "${BACKSTAGE_PASSES}"`, function () {
                    describe('sellIn', function () {
                        it('should decrease by 1', function () {
                            const item = setupItem(NAME, 1, 1);
                            expect(item.sellIn).toBe(0);
                        });

                        it('should go negative', function () {
                            const item = setupItem(NAME, 0, 1);
                            expect(item.sellIn).toBe(-1);
                        });
                    });

                    describe('quality', function () {
                        it('should increase by 1', function () {
                            const item = setupItem(BACKSTAGE_PASSES, 11, 1);
                            expect(item.quality).toBe(2);
                        });

                        it('should not go over 50', function () {
                            const item = setupItem(BACKSTAGE_PASSES, 1, 50);
                            expect(item.quality).toBe(50);
                        });

                        describe('sellIn 10 or lower', function () {
                            it('should increase by 2', function () {
                                const item = setupItem(BACKSTAGE_PASSES, 10, 1);
                                expect(item.quality).toBe(3);
                            });

                            it('should not go over 50', function () {
                                const item = setupItem(BACKSTAGE_PASSES, 1, 49);
                                expect(item.quality).toBe(50);
                            });
                        });

                        describe('sellIn 5 or lower', function () {
                            it('should increase by 3', function () {
                                const item = setupItem(BACKSTAGE_PASSES, 5, 1);
                                expect(item.quality).toBe(4);
                            });

                            it('should not go over 50', function () {
                                const item = setupItem(BACKSTAGE_PASSES, 1, 48);
                                expect(item.quality).toBe(50);
                            });
                        });

                        describe('sellIn 0 or lower', function () {
                            it('should be 0', function () {
                                const item = setupItem(BACKSTAGE_PASSES, 0, 1);
                                expect(item.quality).toBe(0);
                            });
                        });
                    });
                });
            });

            describe('ConjuredItem', function () {
                describe('sellIn', function () {
                    it('should decrease by 1', function () {
                        const item = setupConjuredItem(NAME, 1, 1);
                        expect(item.sellIn).toBe(0);
                    });

                    it('should go negative', function () {
                        const item = setupConjuredItem(NAME, 0, 1);
                        expect(item.sellIn).toBe(-1);
                    });
                });

                describe('quality', function () {
                    it('should decrease by 2', function () {
                        const item = setupConjuredItem(NAME, 1, 3);
                        expect(item.quality).toBe(1);
                    });

                    it('should not go negative', function () {
                        const item = setupConjuredItem(NAME, 1, 0);
                        expect(item.quality).toBe(0);
                    });

                    describe('sell date passed', function () {
                        it('should decrease by 4', function () {
                            const item = setupConjuredItem(NAME, 0, 5);
                            expect(item.quality).toBe(1);
                        });
                    });
                });

                describe(`named "${AGED_BRIE}"`, function () {
                    describe('sellIn', function () {
                        it('should decrease by 1', function () {
                            const item = setupConjuredItem(NAME, 1, 1);
                            expect(item.sellIn).toBe(0);
                        });

                        it('should go negative', function () {
                            const item = setupConjuredItem(NAME, 0, 1);
                            expect(item.sellIn).toBe(-1);
                        });
                    });

                    describe('quality', function () {
                        it('should increase by 2', function () {
                            const item = setupConjuredItem(AGED_BRIE, 1, 1);
                            expect(item.quality).toBe(3);
                        });

                        it('should not go over 50', function () {
                            const item = setupConjuredItem(AGED_BRIE, 1, 50);
                            expect(item.quality).toBe(50);
                        });

                        describe('sell date passed', function () {
                            it('should increase by 4', function () {
                                const item = setupConjuredItem(AGED_BRIE, 0, 1);
                                expect(item.quality).toBe(5);
                            });
                        });
                    });
                });

                describe(`named "${SULFURAS}"`, function () {
                    describe('sellIn', function () {
                        it('should not change', function () {
                            const item = setupConjuredItem(SULFURAS, 1, 1);
                            expect(item.sellIn).toBe(1);
                        });
                    });

                    describe('quality', function () {
                        it('should not change', function () {
                            const item = setupConjuredItem(SULFURAS, 1, 1);
                            expect(item.quality).toBe(1);
                        });
                    });
                });

                describe(`named "${BACKSTAGE_PASSES}"`, function () {
                    describe('sellIn', function () {
                        it('should decrease by 1', function () {
                            const item = setupConjuredItem(NAME, 1, 1);
                            expect(item.sellIn).toBe(0);
                        });

                        it('should go negative', function () {
                            const item = setupConjuredItem(NAME, 0, 1);
                            expect(item.sellIn).toBe(-1);
                        });
                    });

                    describe('quality', function () {
                        it('should increase by 2', function () {
                            const item = setupConjuredItem(BACKSTAGE_PASSES, 11, 1);
                            expect(item.quality).toBe(3);
                        });

                        it('should not go over 50', function () {
                            const item = setupConjuredItem(BACKSTAGE_PASSES, 1, 50);
                            expect(item.quality).toBe(50);
                        });

                        describe('sellIn 10 or lower', function () {
                            it('should increase by 4', function () {
                                const item = setupConjuredItem(BACKSTAGE_PASSES, 10, 1);
                                expect(item.quality).toBe(5);
                            });

                            it('should not go over 50', function () {
                                const item = setupConjuredItem(BACKSTAGE_PASSES, 1, 48);
                                expect(item.quality).toBe(50);
                            });
                        });

                        describe('sellIn 5 or lower', function () {
                            it('should increase by 6', function () {
                                const item = setupConjuredItem(BACKSTAGE_PASSES, 5, 1);
                                expect(item.quality).toBe(7);
                            });

                            it('should not go over 50', function () {
                                const item = setupConjuredItem(BACKSTAGE_PASSES, 1, 46);
                                expect(item.quality).toBe(50);
                            });
                        });

                        describe('sellIn 0 or lower', function () {
                            it('should be 0', function () {
                                const item = setupConjuredItem(BACKSTAGE_PASSES, 0, 1);
                                expect(item.quality).toBe(0);
                            });
                        });
                    });
                });
            });
        });
    });
});
