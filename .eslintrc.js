module.exports = {
    'env': {
        'commonjs': true,
        'es6': true,
        'node': true
    },
    'extends': 'eslint:recommended',
    'globals': {
        'Atomics': 'readonly',
        'SharedArrayBuffer': 'readonly'
    },
    'parserOptions': {
        'ecmaVersion': 2018
    },
    'rules': {
        quotes: ['error', 'single', { 'allowTemplateLiterals': true }],
        'no-fallthrough': 'off',
        'object-curly-spacing': ['error', 'always'],
        'semi-style': ['error', 'last']
    },
    overrides: [{
        files: ['**/*.test.js'],
        env: {
            jest: true
        }
    }]
};
