const AGED_BRIE = 'Aged Brie';
const BACKSTAGE_PASSES = 'Backstage passes to a TAFKAL80ETC concert';
const SULFURAS = 'Sulfuras, Hand of Ragnaros';

/** Class representing an item. */
class Item {
    /**
     * Create an item.
     * @param {string} name
     * @param {number} sellIn
     * @param {number} quality
     */
    constructor (name, sellIn, quality) {
        this.name = name;
        this.sellIn = sellIn;
        this.quality = quality;
    }
}

/** Class representing a conjured item. */
class ConjuredItem extends Item {
    /**
     * Create a conjured item.
     * @param {string} name
     * @param {number} sellIn
     * @param {number} quality
     */
    constructor (name, sellIn, quality) {
        super(name, sellIn, quality);
        this.multiplier = 2;
    }
}

/** Class representing a shop. */
class Shop {
    /**
     * Create a shop.
     * @param {object[]} [items=[]]
     */
    constructor (items = []) {
        this.items = items;
    }

    /**
     * Updates quality of all items in shop.
     * @returns {object[]} Updated items.
     */
    updateQuality() {
        this.items = this.items.map(updateItem);

        return this.items;
    }
}

function updateItem(item) {
    if (item.name === SULFURAS) {
        return item;
    }

    if ([AGED_BRIE, BACKSTAGE_PASSES].includes(item.name)) {
        item.quality = incrementQuality(item);

        if (item.name === BACKSTAGE_PASSES) {
            if (item.sellIn <= 10) {
                item.quality = incrementQuality(item);
            }
            if (item.sellIn <= 5) {
                item.quality = incrementQuality(item);
            }
        }
    } else {
        item.quality = decrementQuality(item);
    }

    item.sellIn -= 1;

    if (item.sellIn < 0) {
        if (item.name === BACKSTAGE_PASSES) {
            item.quality = 0;
        } else if (item.name === AGED_BRIE) {
            item.quality = incrementQuality(item);
        } else {
            item.quality = decrementQuality(item);
        }
    }

    return item;
}

function incrementQuality({ quality, multiplier = 1 }) {
    if (quality < 50) {
        return quality + (1 * multiplier);
    }

    return quality;
}

function decrementQuality({ quality, multiplier = 1 }) {
    if (quality > 0) {
        return quality - (1 * multiplier);
    }

    return quality;
}

module.exports = {
    Item,
    ConjuredItem,
    Shop
}
